<?php
class Animal {
    public $name;
    public $legs;
    public $cold_blooded;

    public function set_name($name){
        $this->name = $name;
    }
    public function set_legs($legs){
        $this->legs = $legs;
    }
    public function set_cold_blooded($cold_blooded){
        $this->cold_blooded = $cold_blooded;
    }
}  




?>