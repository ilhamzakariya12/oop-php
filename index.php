<?php 
require "animal.php";

$sheep = new Animal("Shaun");
$sheep->set_name("Shaun");
$sheep->set_legs(2);
$sheep->set_cold_blooded("False");

echo "Sheep"; echo "<br>";
echo "Name : ". $sheep->name; echo "<br>"; // "shaun"
echo "Legs : ". $sheep->legs; echo "<br>"; // 2
echo "Cold blooded: ". $sheep->cold_blooded; echo "<br><br>"; // false

require "Frog.php";

$kodok = new Frog("Buduk");
$kodok->set_name("Buduk");
$kodok->set_legs(4);
$kodok->set_jump("hop hop");

echo "Frog"; echo "<br>";
echo "Name : ". $kodok->name; echo "<br>";
echo "Legs : ". $kodok->legs; echo "<br>";
echo "Skill : ". $kodok->jump; echo "<br><br>";

require "Ape.php";

$sungokong = new Ape("Kera Sakti");
$sungokong->set_name("Kera Sakti");
$sungokong->set_legs(2);
$sungokong->set_yell("Auooo");

echo "Ape"; echo "<br>";
echo "Name : ". $sungokong->name; echo "<br>";
echo "Legs : ". $sungokong->legs; echo "<br>";
echo "Skill : Yelling ". $sungokong->yell;



?>